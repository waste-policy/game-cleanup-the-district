import { Game } from './js/Game/Game'
import './sass/main.sass'
import { GAME_ELEMENT_ID } from './js/Settings'

new Game(GAME_ELEMENT_ID)
