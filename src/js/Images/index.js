import DefaultBackgroundImage from '../../images/backgrounds/default-background.png'
import DefaultBackgroundClouds from '../../images/backgrounds/cloud.svg'
import DefaultBackgroundSun from '../../images/backgrounds/sun.svg'
import DefaultBackgroundTree from '../../images/backgrounds/tree.png'
import DefaultBackgroundGrass from '../../images/backgrounds/grass.svg'
import DefaultBackgroundClover from '../../images/backgrounds/clover.png'
import TransparentBin from '../../images/bins/bin-transparent.svg'
import BlueBin from '../../images/bins/bin-blue.svg'
import GreenBin from '../../images/bins/bin-green.svg'
import YellowBin from '../../images/bins/bin-yellow.svg'
import TransparentBag from '../../images/bags/bag-transparent.svg'
import BlueBag from '../../images/bags/bag-blue.svg'
import GreenBag from '../../images/bags/bag-green.svg'
import YellowBag from '../../images/bags/bag-yellow.svg'
import PSZOKImage from '../../images/other-waste-collectors/pszok.png'
import BananaImage from '../../images/wastes/banana.png'
import BoneImage from '../../images/wastes/bone.png'
import BottleImage from '../../images/wastes/bottle.png'
import CanImage from '../../images/wastes/can.png'
import NewspaperImage from '../../images/wastes/newspaper.png'


const IMAGES_TO_PRELOAD = [
    DefaultBackgroundImage,
    DefaultBackgroundClouds,
    DefaultBackgroundSun,
    DefaultBackgroundTree,
    DefaultBackgroundGrass,
    DefaultBackgroundClover,
    TransparentBin,
    BlueBin,
    GreenBin,
    YellowBin,
    TransparentBag,
    BlueBag,
    GreenBag,
    YellowBag,
    PSZOKImage,
    BananaImage,
    BoneImage,
    BottleImage,
    CanImage, 
    NewspaperImage,
]

export {
    IMAGES_TO_PRELOAD,
    DefaultBackgroundImage,
    DefaultBackgroundClouds,
    DefaultBackgroundSun,
    DefaultBackgroundTree,
    DefaultBackgroundGrass,
    DefaultBackgroundClover,
    TransparentBin,
    BlueBin,
    GreenBin,
    YellowBin,
    TransparentBag,
    BlueBag,
    GreenBag,
    YellowBag,
    PSZOKImage,
    BananaImage,
    BoneImage,
    BottleImage,
    CanImage, 
    NewspaperImage
}
