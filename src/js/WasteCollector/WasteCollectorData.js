class WasteCollectorData {
    constructor(wasteCollectorData) {
        this.id = wasteCollectorData.id
        this.type = wasteCollectorData.type
        this.color = wasteCollectorData.color
        this.categories = wasteCollectorData.categories
        this.isSeasonSpecified = wasteCollectorData.isSeasonSpecified
        this.season = wasteCollectorData.season
        this.name = wasteCollectorData.name
    }

    /**
     * Returns ID of waste collector
     *
     * @returns {number} - waste collector ID
     */
    getId() {
        return this.id || null
    }

    getType() {
        return this.type || null
    }

    getColor() {
        return this.color || null
    }

    getCategories() {
        return this.categories || []
    }

    getIsSeasonSpecified() {
        return this.isSeasonSpecified || null
    }

    getSeason() {
        return this.season || null
    }

    getName() {
        return this.name || null
    }
}

export { WasteCollectorData }
