import { WasteCollector } from './WasteCollector'
import { WasteCollectorData } from './WasteCollectorData'

export { WasteCollector, WasteCollectorData }
