import { GameObject } from '../Game'
import {
    TransparentBin,
    BlueBin,
    GreenBin,
    YellowBin,
    TransparentBag,
    BlueBag,
    GreenBag,
    YellowBag,
    PSZOKImage,
} from '../Images'
import { WasteCollectorData } from '.'

class WasteCollector extends GameObject {
    /**
     * Initialize waste collector object
     *
     * @param {WasteCollectorData} wasteCollectorData - waste collector data
     * @param {number} x - x coordinate in percents
     * @param {number} y - y coordinate in percents
     * @param {number} scale - scale in percent of object height according to game window height
     * @param {boolean} countFromBottom - defines if y coordinate should be counted from bottom
     * @param {boolean} countFromRight - defines if x coordinate should be counted from right
     */
    constructor(
        wasteCollectorData,
        x,
        y,
        scale,
        countFromRight,
        countFromBottom
    ) {
        super(null, x, y, scale, countFromRight, countFromBottom)
        this.wasteCollectorData = wasteCollectorData

        this.setImageUrl(
            this.getWasteCollectorImageUrl({
                type: wasteCollectorData.getType(),
                color: wasteCollectorData.getColor(),
            })
        )
        this.generateGameObject()
    }

    /**
     * Returns waste collector data
     * 
     * @returns {WasteCollectorData} - waste collector data
     */
    getWasteCollectorData(){
        return this.wasteCollectorData
    }

    /**
     * Returns waste collector image URL
     * 
     * @param {string} type - type of waste collector
     * @param {color} color - color of waste collector
     * @returns {string} - image URL
     */
    getWasteCollectorImageUrl({type, color}) {
        switch (type) {
            case 'bag':
                return this.getWasteBagImageUrl(color)
            case 'bin':
                return this.getWasteBinImageUrl(color)
            case 'pszok':
                return PSZOKImage
        }
    }

    /**
     * Returns waste bag image URL
     * 
     * @param {string} color - bag color
     * @returns {string} - bag image URL
     */
    getWasteBagImageUrl(color) {
        switch (color) {
            case 'green':
                return GreenBag
            case 'yellow':
                return YellowBag
            case 'blue':
                return BlueBag
            default:
                return TransparentBag
        }
    }

    /**
     * Returns waste bin image URL
     * 
     * @param {string} color - bin color
     * @returns {string} - bin image URL
     */
    getWasteBinImageUrl(color) {
        switch (color) {
            case 'green':
                return GreenBin
            case 'yellow':
                return YellowBin
            case 'blue':
                return BlueBin
            default:
                return TransparentBin
        }
    }
}

export { WasteCollector }
