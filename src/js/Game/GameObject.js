import { PIXI } from '../PIXI'
import { IMAGES, DIMENSIONS } from '../Common'

class GameObject {
    /**
     * Initialize game object
     *
     * @param {string} imageURL - URL of the image
     * @param {number} x - x coordinate in percents
     * @param {number} y - y coordinate in percents
     * @param {number} scale - scale in percent of object height according to game window height
     * @param {boolean} countFromBottom - defines if y coordinate should be counted from bottom
     * @param {boolean} countFromRight - defines if x coordinate should be counted from right
     */
    constructor(imageUrl, x, y, scale, countFromRight, countFromBottom) {
        this.bindedObject = null
        this.gameObject = null
        this.imageUrl = imageUrl
        this.x = x
        this.y = y
        this.scale = scale
        this.countFromBottom = countFromBottom
        this.countFromRight = countFromRight
    }

    /**
     * Gets game object
     *
     * @returns {PIXI.Sprite} game object
     */
    getGameObject() {
        return this.gameObject
    }

    /**
     * Sets image url
     * @param {string} imageUrl - URL to image
     */
    setImageUrl(imageUrl){
        this.imageUrl = imageUrl
    }

    /**
     * creates a game object
     */
    generateGameObject() {
        const image = IMAGES.generateFromUrl(this.imageUrl)

        const texture = PIXI.Texture.from(image)
        texture.baseTexture.scaleMode = PIXI.SCALE_MODES.LINEAR
        texture.baseTexture.mipmap = true

        const sprite = new PIXI.Sprite(texture)

        const scaleRatio = this.getScaleRatio(image.height, this.scale)

        sprite.scale.set(scaleRatio)

        sprite.position.x = this.getPixelFromLeftFromPercent(
            this.x,
            this.countFromRight,
            image.width * scaleRatio
        )
        sprite.position.y = this.getPixelFromTopFromPercent(
            this.y,
            this.countFromBottom,
            image.height * scaleRatio
        )

        if (this.scale === 100 && image.width * scaleRatio < DIMENSIONS.getGameWidth()) {
            sprite.width = DIMENSIONS.getGameWidth()
        }

        this.gameObject = sprite
    }

    /**
     * Gets scale ratio
     *
     * @param {number} objectHeight - current height of the object
     * @param {number} scale - destiny height in percent of game window height
     *
     * @returns {number} - scale to pass resizing correctly
     */
    getScaleRatio(objectHeight, scale) {
        const heightRatio = DIMENSIONS.getGameHeight() / (100 / scale)
        return heightRatio / objectHeight
    }

    /**
     * Gets pixels value from left side. If right-sided the value is still counted from left
     * but with corretions
     *
     * @param {number} percent - percent value from side
     * @param {boolean} alignRight - defines if object is aligned to right side
     * @param {number} elementWidth - width of element to corrently pass when is right-sided
     */
    getPixelFromLeftFromPercent(percent, alignRight, elementWidth) {
        const pixelsFromLeft = DIMENSIONS.getGameWidth() / (100 / percent)

        if (alignRight) {
            return DIMENSIONS.getGameWidth() - elementWidth - pixelsFromLeft
        }

        return pixelsFromLeft
    }

    /**
     * Gets pixels value from top side. If bottom-sided the value is still counted from top
     * but with corretions
     *
     * @param {number} percent - percent value from side
     * @param {boolean} alignBottom - defines if object is aligned to bottom side
     * @param {number} elementHeight - width of element to corrently pass when is bottom-sided
     */
    getPixelFromTopFromPercent(percent, alignBottom, elementHeight) {
        const pixelsFromTop = DIMENSIONS.getGameHeight() / (100 / percent)

        if (alignBottom) {
            return DIMENSIONS.getGameHeight() - elementHeight - pixelsFromTop
        }

        return pixelsFromTop
    }
}

export { GameObject }
