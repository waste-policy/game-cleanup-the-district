import { PIXI } from '../PIXI'
import { IMAGES_TO_PRELOAD } from '../Images'
import { DIMENSIONS } from '../Common'
import { GameObject, AvailableGameObjects } from '.'
import { WelcomeStage, Stage } from '../Stages'
import { StageData } from '../Stages/StageData'

class Game {
    /**
     * Creates a game instance
     * @param {string} elementId - ID of HTML element to place the game
     */
    /* TODO: elements to preload - methods */
    constructor(elementId) {
        this.app = null
        this.elementId = elementId
        this.elementsToPreload = IMAGES_TO_PRELOAD
        this.stages = []
        this.currentStage = 1
        this.availableGameObjects = new AvailableGameObjects()
        this.isUserResizing = false

        this.createGameApplication()
        this.preload(this.elementsToPreload, this.initializeGameAndStart)
    }

    /**
     * Returns all available
     */
    getAvailableGameObjects(){
        return this.availableGameObjects
    }

    preload(elements, callback){
        new PIXI.Loader()
            .add(elements)
            .load(callback.bind(this))
    }

    /**
     * Initializes game view, resizing, stages and starts the game
     */
    initializeGameAndStart() {
        this.initializeGameView()
        this.initializeResizing()
        this.initializeStages()
        this.start()
    }

    /**
     * Start game on set stage number
     */
    start() {
        this.clearContainer()
        this.initializeContainer()
        this.stages[this.currentStage - 1].play()
    }

    /**
     * Changes current stage to next if present and initializes start
     */
    nextLevel() {
        if (this.currentStage < this.stages.length) {
            this.currentStage++
            this.start()
        }
    }

    /**
     * Sets all stages
     */
    initializeStages() {
        // this.stages = [new WelcomeStage(this)]

        for(let stage of this.getAvailableGameObjects().getStages()){
            this.stages.push(new Stage(this, stage))
        }
    }

    /**
     * Shows given game objects on the screen
     *
     * @param {Array<GameObject>} gameObjects - array of gameObjects
     */
    show(gameObjects) {
        for (var gameObject of gameObjects) {
            this.app.stage.addChild(gameObject.getGameObject())
        }
    }

    /**
     * Shows texts on the screen
     *
     * @param {Array<PIXI.Text} texts - array of texts
     */
    showTexts(texts) {
        for (var text of texts) {
            this.app.stage.addChild(text)
        }
    }

    /**
     * Initialize PIXI.Application instance
     */
    createGameApplication() {
        this.app = new PIXI.Application({
            autoResize: true,
            backgroundColor: 0x1099bb,
            resolution: window.devicePixelRatio || 1,
        })
    }

    /**
     * Clears the whole area
     */
    clearContainer() {
        this.app.stage.removeChildren()
    }

    /**
     * Initializes container
     */
    initializeContainer() {
        this.app.stage.addChild(new PIXI.Container())
    }

    /**
     * Places game view to given HTML container by elementId
     */
    initializeGameView() {
        document.querySelector(`#${this.elementId}`).appendChild(this.app.view)
    }

    /**
     * Initializes window resizing
     */
    initializeResizing() {
        this.resizeGameCanvas()
        window.addEventListener(
            'resize',
            this.resizeGameEventListener.bind(this)
        )
    }

    /**
     * Resizes game render
     */
    /* TODO: Add min-max values check */
    resizeGameCanvas() {
        this.app.renderer.resize(
            DIMENSIONS.getWindowWidth(),
            DIMENSIONS.getWindowHeight()
        )
    }

    /**
     * Resize game objects
     */
    resizeGameEventListener() {
        if (!this.isUserResizing) {
            this.isUserResizing = true

            window.setTimeout(
                function() {
                    this.resizeGameCanvas()
                    this.start()
                    this.isUserResizing = false
                }.bind(this),
                1500
            )
        }
    }
}

export { Game }
