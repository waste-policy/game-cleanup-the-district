import {
    OfflineDistrictInfo,
    OfflineWastesInfo,
    OfflineStagesInfo,
} from '../Settings'
import { Waste, WasteData } from '../Waste'
import { WasteCollectorData } from '../WasteCollector'
import { StageData } from '../Stages/StageData'

/**
 * Class of Available game objects from online or offline storage
 */
/* TODO: online check */
class AvailableGameObjects {
    /**
     * Creates a game settings instance
     */
    constructor() {
        this.wasteCollectors = []
        this.wastes = []
        this.stages = []

        this.initializeWasteCollectors()
        this.initializeWastes()
        this.initializeStages(this)
    }

    findWaste(wasteId){
        return this.wastes.find(waste => waste.getId() == wasteId)
    }

    findWasteCollector(wasteCollectorId){
        return this.wasteCollectors.find(wasteCollector => wasteCollector.getId() == wasteCollectorId)
    }

    /**
     * Checks for info about bins
     */
    initializeWasteCollectors() {
        let wasteCollectors = this.getOfflineWasteCollectorsData()

        this.setWasteCollectors(
            wasteCollectors.map(wasteBin => new WasteCollectorData(wasteBin))
        )
    }

    /**
     * Check for info about wastes
     */
    initializeWastes() {
        let wastes = this.getOfflineWastesData()
        this.setWastes(wastes.map(waste => new WasteData(waste)))
    }

    /**
     * Check for info about stages
     */
    initializeStages(availableGameObjects) {
        let stages = this.getOfflineStagesData()
        this.setStages(stages.map(stage => new StageData(stage, availableGameObjects)))
    }

    /**
     * Gets offline info about collectors
     *
     * @returns {Array<object>} - offline collectors data
     */
    getOfflineWasteCollectorsData() {
        return OfflineDistrictInfo.collectors
    }

    /**
     * Sets offline info about wastes
     *
     * @returns {Array<object>} - offline wastes data
     */
    getOfflineWastesData() {
        return OfflineWastesInfo.wastes
    }


    /**
     * Sets offline info about stages
     *
     * @returns {Array<object>} - offline stages data
     */
    getOfflineStagesData() {
        return OfflineStagesInfo.stages
    }

    /**
     * Sets waste collectors
     *
     * @param {Array<WasteCollectorData>} wasteCollectors - waste collectors
     */
    setWasteCollectors(wasteCollectors) {
        this.wasteCollectors = wasteCollectors
    }

    /**
     * Sets wastes
     *
     * @param {Array<object>} wastes - wastes
     */
    setWastes(wastes) {
        this.wastes = wastes
    }

    /**
     * Sets stages
     *
     * @param {Array<StageData>} stages - stages
     */
    setStages(stages) {
        this.stages = stages
    }

    /**
     * Returns waste collectors
     *
     * @returns {Array<WasteCollector>} - waste collectors
     */
    getWasteCollectors() {
        return this.wasteCollectors
    }

    /**
     * Returns wastes
     *
     * @returns {Array<Waste>} - wastes
     */
    getWastes() {
        return this.wastes
    }
    /**
     * Returns stages
     *
     * @returns {Array<Stage>} - stages
     */
    getStages() {
        return this.stages
    }
}

export { AvailableGameObjects }
