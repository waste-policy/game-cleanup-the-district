import OfflineDistrictInfo from './district_info.json'
import OfflineWastesInfo from './wastes_info.json'
import OfflineStagesInfo from './stages_info.json'

const MIN_WIDTH = 1200
const MIN_HEIGHT = 600

const MAX_WIDTH = 2560
const MAX_HEIGHT = 1440

const MAX_RESPONSE_WAITING_TIME = 30 * 1000

const DISTRICT_INFO_URL = ''
const CUSTOM_LEVELS_URL = ''

const GAME_ELEMENT_ID = 'game'

const NAME = 'Segregacja odpadów'


export {
    OfflineDistrictInfo,
    OfflineWastesInfo,
    OfflineStagesInfo,
    MIN_WIDTH,
    MIN_HEIGHT,
    MAX_WIDTH,
    MAX_HEIGHT,
    MAX_RESPONSE_WAITING_TIME,
    DISTRICT_INFO_URL,
    CUSTOM_LEVELS_URL,
    GAME_ELEMENT_ID,
    NAME,
}
