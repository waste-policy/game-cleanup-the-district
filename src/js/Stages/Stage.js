import { BaseStage } from '.'

class Stage extends BaseStage {
    /**
     * Initializes stage
     *
     * @param {Game} game
     */
    constructor(game, stageData) {
        super(game)

        this.setBackgrounds(stageData.getBackgrounds())
        // this.setWasteCollectors(this.generateDefaultWasteCollectors())
        this.setWasteCollectors(stageData.getWasteCollectors())
        this.addListenersToWastes(stageData.getWastes())
        this.setWastes(stageData.getWastes())
    }

    addListenersToWastes(wastes) {
        for (let waste of wastes) {
            waste.onWasteDrop = this.onWasteDrop.bind(this)
            waste.onWasteMove = this.onWasteMove.bind(this)
        }
    }
}

export { Stage }
