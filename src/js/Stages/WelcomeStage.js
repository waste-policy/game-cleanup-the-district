import { Background } from '../Background'
import { BaseStage } from '.'
import { DefaultBackgroundImage } from '../Images'
import { PIXI } from '../PIXI'
import { NAME } from '../Settings'

class WelcomeStage extends BaseStage {
    /**
     * Initialize welcome screen
     * 
     * @param {Game} game - game instance
     */
    constructor(game) {
        super(game)

        this.setBackgrounds(this.generateBackgrounds())
        this.setTexts(this.generateTexts())
    }

    /**
     * Generates background image
     * 
     * @returns {Array<Background>} - array of backgrounds
     */
    generateBackgrounds() {
        return [
            new Background(DefaultBackgroundImage, 0, 0, 100, false, false),
        ]
    }

    /**
     * Generates texts
     * 
     * @returns {Array<PIXI.Text>} - array of texts
     */
    generateTexts() {
        const style = new PIXI.TextStyle({
            align: 'center',
            fontFamily: 'Arial',
            fontSize: 72,
            // fontStyle: 'italic',
            fontWeight: 'bold',
            fill: ['#fff', '#ccc'], // gradient
            stroke: '#333',
            strokeThickness: 5,
            dropShadow: true,
            dropShadowColor: '#000000',
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 4,
            wordWrap: true,
            wordWrapWidth: 800,
        })

        const richText = new PIXI.Text(NAME, style)
        richText.x = window.innerWidth / 2 - richText.width / 2
        richText.y = richText.height

        const playStyle = new PIXI.TextStyle({
            align: 'center',
            fontFamily: 'Arial',
            fontSize: 60,
            // fontStyle: 'italic',
            fontWeight: 'bold',
            fill: ['#fff', '#ccc'], // gradient
            stroke: '#333',
            strokeThickness: 5,
            dropShadow: true,
            dropShadowColor: '#000000',
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 4,
            wordWrap: true,
            wordWrapWidth: 800,
        })

        const playText = new PIXI.Text('GRAJ', playStyle)
        playText.x = window.innerWidth / 2 + 50
        playText.y = richText.height * 4

        playText.interactive = true
        playText.buttonMode = true
        playText.on('pointerdown', this.game.nextLevel.bind(this.game))
        playText.on('pointerover', function() {
            this.style.dropShadowColor = '#FFFFFF'
        })
        playText.on('pointerout', function() {
            this.style.dropShadowColor = '#000000'
        })

        return [richText, playText]
    }
}

export { WelcomeStage }
