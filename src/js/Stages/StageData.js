import DefaultBackgroundImage from '../../images/backgrounds/default-background.png'
import DefaultBackgroundClouds from '../../images/backgrounds/cloud.svg'
import DefaultBackgroundSun from '../../images/backgrounds/sun.svg'
import DefaultBackgroundTree from '../../images/backgrounds/tree.png'
import DefaultBackgroundGrass from '../../images/backgrounds/grass.svg'
import DefaultBackgroundClover from '../../images/backgrounds/clover.png'
import BananaImage from '../../images/wastes/banana.png'
import BoneImage from '../../images/wastes/bone.png'
import BottleImage from '../../images/wastes/bottle.png'
import CanImage from '../../images/wastes/can.png'
import NewspaperImage from '../../images/wastes/newspaper.png'
import { Background } from '../Background'
import { Waste } from '../Waste'
import { WasteCollector } from '../WasteCollector'

class StageData {
    constructor(stageData, availableGameObjects) {
        this.backgrounds = []
        this.wastes = []
        this.wasteCollectors = []

        this.setBackgrounds(stageData)
        this.setWastes(stageData.wastes, availableGameObjects)
        this.setWasteCollectors(stageData.wasteCollectors, availableGameObjects)
    }

    getBackgrounds() {
        return this.backgrounds
    }

    getWastes() {
        return this.wastes
    }

    getWasteCollectors() {
        return this.wasteCollectors
    }

    setBackgrounds({ backgrounds }) {
        this.backgrounds = backgrounds.map(
            background =>
                new Background(
                    this.getBackgroundImage(background.imageUrl),
                    background.x,
                    background.y,
                    background.scale,
                    background.countFromRight,
                    background.countFromBottom
                )
        )
    }

    setWastes(wastes, availableGameObjects) {
        this.wastes = wastes.map(
            waste =>
                new Waste(
                    availableGameObjects.findWaste(waste.id),
                    waste.x,
                    waste.y,
                    waste.scale,
                    waste.countFromRight,
                    waste.countFromBottom,
                    null,
                    null
                )
        )
    }

    setWasteCollectors(wasteCollectors, availableGameObjects) {
        this.wasteCollectors = wasteCollectors.map(
            wasteCollector =>
                new WasteCollector(
                    availableGameObjects.findWasteCollector(wasteCollector.id),
                    wasteCollector.x,
                    wasteCollector.y,
                    wasteCollector.scale,
                    wasteCollector.countFromRight,
                    wasteCollector.countFromBottom
                )
        )
    }

    getBackgroundImage(backgroundUrl) {
        switch (backgroundUrl) {
            case 'default':
                return DefaultBackgroundImage
            case 'sun':
                return DefaultBackgroundSun
            case 'cloud':
                return DefaultBackgroundClouds
            case 'tree':
                return DefaultBackgroundTree
            case 'grass':
                return DefaultBackgroundGrass
            case 'clover':
                return DefaultBackgroundClover
            default:
                return backgroundUrl
        }
    }

    getWasteImage(wasteName) {
        switch (wasteName) {
            case 'banana':
                return BananaImage
            case 'bone':
                return BoneImage
            case 'bottle':
                return BottleImage
            case 'can':
                return CanImage
            case 'newspaper':
                return NewspaperImage
            default:
                return wasteName
        }
    }
}

export { StageData }
