import { PIXI } from '../PIXI'
import { DropShadowFilter } from '@pixi/filter-drop-shadow'
import { WasteCollector } from '../WasteCollector/WasteCollector'
import { Game } from '../Game'
import { Background } from '../Background'
import { Garbage } from '../Waste/Waste'

class BaseStage {
    /**
     * Stage initializer
     *
     * @param {Game} game - game instance
     */
    constructor(game) {
        this.game = game
        this.wasteCollectors = []
        this.wastes = []
        this.backgrounds = []
        this.texts = []
        this.lastInteractedWasteCollector = null
        this.lastInteractedWaste = null
        this.numberOfDroppedWastes = 0
    }

    /**
     * Checks if all of wasted are dropped
     * 
     * @returns {boolean} - is level completed
     */
    isCompleted(){
        return this.wastes.length === this.numberOfDroppedWastes
    }

    /**
     * Handler of dropping waste
     */
    wasteDropped(waste){
        this.numberOfDroppedWastes++

        waste.getGameObject().destroy()

        if(this.isCompleted()){
            this.game.nextLevel()
        }
    }

    /**
     * Sets backgrounds array
     *
     * @param  {Array<Background>} backgrounds - array of backgrounds
     */
    setBackgrounds(backgrounds) {
        this.backgrounds = backgrounds
    }

    /**
     * Sets wastes array
     *
     * @param  {Array<WasteData>} wastes - array of garbages
     */
    setWastes(wastes) {
        this.wastes = wastes
    }

    /**
     * Sets game texts array
     *
     * @param  {Array<PIXI.Text>} texts - array of game texts
     */
    setTexts(texts) {
        this.texts = texts
    }

    /**
     * Sets waste collectors array
     *
     * @param  {Array<WasteCollector>} wasteCollectors - array of waste collectors
     */
    setWasteCollectors(wasteCollectors) {
        this.wasteCollectors = wasteCollectors
    }

    /**
     * Generates all waste collectors
     *
     * @returns {Array<WasteCollector>} - generated waste collectors
     */
    generateDefaultWasteCollectors() {
        return this.game.getAvailableGameObjects().getWasteCollectors().map((wasteCollector, index) =>
            new WasteCollector(wasteCollector, 14 * index + 2, 2, 18, false, true)
        )
    }

    /**
     * Show everything on screen and starts the stage
     */
    play() {
        this.game.show(this.backgrounds)
        this.game.show(this.wasteCollectors)
        this.game.show(this.wastes)
        this.game.showTexts(this.texts)
    }

    /**
     * Handler of moving waste
     *
     * @param {Waste} waste
     */
    onWasteMove(waste) {
        const collidedBin = this.getCollidedBin(waste)
        const shadow = new DropShadowFilter({
            color: 0xffffff,
            blur: 3,
            distance: 0,
            pixelSize: 1,
            alpha: 1,
            quality: 4,
        })

        if (collidedBin && collidedBin === this.lastInteractedWasteCollector) {
            collidedBin.getGameObject().filters = [shadow]
        } else {
            if (this.lastInteractedWasteCollector) {
                this.lastInteractedWasteCollector.getGameObject().filters = []
            }
        }

        this.lastInteractedWasteCollector = collidedBin
    }

    /**
     * Handler of dropped waste
     */
    onWasteDrop(that) {
        if (this.lastInteractedWasteCollector) {
            const waste = that.bindedGameObject.lastInteractedWaste
            if(this.isWasteDroppedAtProperWasteCollector(waste, this.lastInteractedWasteCollector)){
                this.wasteDropped(waste)
            }
        }
    }

    /**
     * Returns collided bin
     *
     * @param {Garbage} garbage - Garbage object
     * @returns {WasteCollector|null} - Collided bin
     */
    getCollidedBin(garbage) {
        const garbageBounds = garbage.getBounds()

        const centralPoint = new PIXI.Point(
            garbageBounds.x + garbageBounds.width / 2,
            garbageBounds.y + garbageBounds.height / 2
        )

        for (var wasteCollector of this.wasteCollectors) {
            if (wasteCollector.getGameObject().containsPoint(centralPoint)) {
                return wasteCollector
            }
        }

        return null
    }

    /**
     * Checks if waste is dropped into properly waste collector
     * 
     * @param {Waste} waste 
     * @param {WasteCollector} wasteCollector 
     * @returns {boolean} - is dropped correctly
     */
    isWasteDroppedAtProperWasteCollector(waste, wasteCollector){
        const wasteCategory = waste.getWasteData().getCategory()
        const wasteCollectorCategories = wasteCollector.getWasteCollectorData().getCategories()
     
        return wasteCollectorCategories.includes(wasteCategory)
    }
}

export { BaseStage }
