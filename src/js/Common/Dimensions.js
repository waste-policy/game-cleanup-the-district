import { GAME_ELEMENT_ID } from '../Settings'

/**
 * Class for common dimensions methods
 */
class Dimensions {
    /**
     * Return width of the game
     *
     * @returns {number} - width of the game
     */
    getGameWidth() {
        const gameElement = document.getElementById(GAME_ELEMENT_ID).firstChild

        if (gameElement) return gameElement.width

        return window.innerWidth
    }

    /**
     * Return height of the game
     *
     * @returns {number} - height of the game
     */
    getGameHeight() {
        const gameElement = document.getElementById(GAME_ELEMENT_ID).firstChild

        if (gameElement) return gameElement.height

        return window.innerHeight
    }

    /**
     * Return width of the window
     *
     * @returns {number} - width of the window
     */
    getWindowWidth() {
        return window.innerWidth
    }

    /**
     * Return height of the window
     *
     * @returns {number} - height of the window
     */
    getWindowHeight() {
        return window.innerHeight
    }
}

const DIMENSIONS = new Dimensions()

export { DIMENSIONS }
