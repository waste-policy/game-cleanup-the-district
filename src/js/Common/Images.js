/**
 * Common class for images methods
 */
class Images {
    /**
     * Generates Image object from given URL
     *
     * @param {string} imageURL - URL of the image
     * @returns {Image} - generated Image object
     */

    generateFromUrl(imageUrl) {
        const image = new Image()
        image.src = imageUrl

        return image
    }
}

const IMAGES = new Images()

export { IMAGES }
