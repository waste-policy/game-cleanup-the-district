import { IMAGES } from './Images'
import { DIMENSIONS } from './Dimensions'

export { IMAGES, DIMENSIONS }