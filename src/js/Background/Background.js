import { GameObject } from '../Game/GameObject'

class Background extends GameObject {
    /**
     * Creates a background instance
     * 
     * @param {string} imageURL 
     * @param {number} x 
     * @param {number} y 
     * @param {number} scale 
     * @param {boolean} countFromBottom 
     * @param {boolean} countFromRight 
     */
    constructor(imageURL, x, y, scale, countFromBottom, countFromRight) {
        super(imageURL, x, y, scale, countFromBottom, countFromRight)
        this.generateGameObject()
    }
}

export { Background }
