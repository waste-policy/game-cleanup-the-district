class WasteData {
    /**
     * Initializer for waste data
     *
     * @param {object} wasteData - waste data
     */
    constructor(wasteData) {
        this.id = wasteData.id
        this.name = wasteData.name
        this.imageUrl = wasteData.imageUrl
        this.category = wasteData.category
    }

    /**
     * Returns ID of waste
     * 
     * @returns {number} - waste ID
     */
    getId() {
        return this.id || null
    }
    
    /**
     * Returns name
     *
     * @returns {string} - name
     */
    getName() {
        return this.name || null
    }

    /**
     * Return category
     *
     * @returns {number} - category ID
     */
    getCategory() {
        return this.category || null
    }

    /**
     * Returns image URL
     *
     * @returns {string} - image URL
     */
    getImageUrl() {
        return this.imageUrl || null
    }
}

export { WasteData }
