import { GameObject } from '../Game'
import {
    BananaImage,
    CanImage,
    BottleImage,
    NewspaperImage,
    BoneImage,
} from '../Images'

/**
 * Class of waste object, extends GameObject
 */
class Waste extends GameObject {
    /**
     * Initialize waste object
     *
     * @param {WasteData} wasteData - waste data
     * @param {number} x - x coordinate in percents
     * @param {number} y - y coordinate in percents
     * @param {number} scale - scale in percent of object height according to game window height
     * @param {boolean} countFromBottom - defines if y coordinate should be counted from bottom
     * @param {boolean} countFromRight - defines if x coordinate should be counted from right
     * @param {function} onWasteDrop - handler of dropped waste
     * @param {function} onWasteMove - hander of moving waste
     */
    constructor(
        wasteData,
        x,
        y,
        scale,
        countFromRight,
        countFromBottom,
        onWasteDrop,
        onWasteMove
    ) {
        super(null, x, y, scale, countFromRight, countFromBottom)
        this.wasteData = wasteData
        this.onWasteDrop = onWasteDrop
        this.onWasteMove = onWasteMove

        this.lastCollidedBin = null
        this.setImageUrl(this.getLocalImage(wasteData.imageUrl))

        this.generateGameObject()
        this.updateGameObject()
    }

    /**
     * Return waste data
     *
     * @returns {WasteData} - waste data
     */
    getWasteData() {
        return this.wasteData
    }

    /**
     * Updates gameObject to handle interactivity
     */
    updateGameObject() {
        this.gameObject.interactive = true
        this.gameObject.buttonMode = true
        this.gameObject.anchor.set(0.5)
        this.gameObject
            .on('pointerdown', this.onDragStart)
            .on('pointerup', this.onDragEnd)
            .on('pointerupoutside', this.onDragEndOutOfWindow)
            .on('pointermove', this.onDragMove)

        this.gameObject.bindedGameObject = this
    }

    /**
     * Check if offline image is present otherwise returns given image URL
     *
     * @param {string} wasteImage - image URL to check
     *
     * @returns {strings} - offline image URL if present otherwise given image URL
     */
    getLocalImage(wasteImage) {
        switch (wasteImage) {
            case 'can':
                return CanImage
            case 'newspaper':
                return NewspaperImage
            case 'bottle':
                return BottleImage
            case 'banana':
                return BananaImage
            case 'bone':
                return BoneImage
            default:
                return garbageImage
        }
    }

    /**
     * Handler of clicked mouse
     *
     * @param {InteractionEvent} event - event of clicked mouse
     */
    onDragStart(event) {
        this.data = event.data
        this.alpha = 0.7
        this.dragging = true
    }

    /**
     * Handler of just non-clicked mouse
     */
    onDragEnd() {
        this.alpha = 1
        this.dragging = false
        // this.data = null

        this.bindedGameObject.lastInteractedWaste = this.bindedGameObject
        this.bindedGameObject.onWasteDrop(this)
    }

    /**
     * Handler of just non-clicked mouse outside window
     */
    onDragEndOutOfWindow() {}

    /**
     * Handler of moving pressed mouse
     */
    onDragMove() {
        if (this.dragging) {
            const newPosition = this.data.getLocalPosition(this.parent)
            this.x = newPosition.x
            this.y = newPosition.y

            this.bindedGameObject.onWasteMove(this)
        }
    }
}

export { Waste }
